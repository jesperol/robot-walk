<?php

namespace Jesperol\RobotTest;

class Robot
{
    const NORTH = 0;
    const EAST = 1;
    const SOUTH = 2;
    const WEST = 3;

    const DIRECTIONS = array(array(0, -1), array(1, 0), array(0, 1), array(-1, 0));

    private $grid;
    private $position;
    private $facing;
    private $blocked = false;

    public function __construct($grid, $position = array(0, 0), $facing = Robot::NORTH)
    {
        $this->grid = $grid;
        $this->position = $position;
        $this->facing = $facing;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function isBlocked()
    {
        return $this->blocked;
    }

    public function executeCommands($commands)
    {
        if ($this->blocked) {
            return;
        }

        foreach (str_split($commands) as $command) {
            $this->executeCommand($command);
            if ($this->blocked) {
                break;
            }
        }
    }

    private function executeCommand($command)
    {
        $reverse = false;
        switch ($command) {
            case 'l':
                $this->facing = ProperMath::mod($this->facing - 1, 4);
                break;
            case 'r':
                $this->facing = ProperMath::mod($this->facing + 1, 4);
                break;
            case 'b':
                $reverse = true;
            case 'f':
                $step = Robot::DIRECTIONS[$this->facing];
                if ($reverse) {
                    $step = array(-$step[0], -$step[1]);
                }
                $next_position = $this->grid->getNextPosition($this->position, $step);
                if ($next_position === false) {
                    $this->blocked = true;
                } else {
                    $this->position = $next_position;
                }
                break;
        }
    }
}
