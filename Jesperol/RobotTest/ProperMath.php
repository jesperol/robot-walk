<?php

namespace Jesperol\RobotTest;

class ProperMath
{

    /**
     * The % operator doesn't behave as many people with a maths background would expect, when dealing with negative numbers.
     * For example, -1 mod 8 = 7, but in PHP, -1 % 8 = -1.
     */
    public static function mod($a, $n)
    {
        return ($a % $n) + ($a < 0 ? $n : 0);
    }
}
