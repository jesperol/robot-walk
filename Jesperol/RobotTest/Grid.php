<?php

namespace Jesperol\RobotTest;

class Grid
{
    private $width;
    private $height;
    private $wraps;
    private $obstacles = array();

    public function __construct($width, $height, $wraps = false)
    {
        $this->width = $width;
        $this->height = $height;
        $this->wraps = $wraps;
    }

    public function addObstacle($position)
    {
        $this->obstacles[] = $position;
    }

    /*
     * Returns next position or false if not a legal move.
     */
    public function getNextPosition($position, $step)
    {
        $next_position = array($position[0] + $step[0], $position[1] + $step[1]);
        if (in_array($next_position, $this->obstacles, true)) {
            return false;
        } else {
            if ($this->wraps) {
                return array(ProperMath::mod($next_position[0], $this->width), ProperMath::mod($next_position[1], $this->height));
            } else {
                return ($next_position[0] < 0 || $next_position[1] < 0 || $next_position[0] >= $this->width || $next_position[1] >= $this->height) ? false : $next_position;
            }
        }
    }
}
