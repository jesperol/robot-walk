<?php

use \Jesperol\RobotTest\Robot;
use \Jesperol\RobotTest\Grid;

// Test Cases
// ----------
// Walk - The robot is on a 100×100 grid at location (0, 0) and facing SOUTH. The robot is given the commands “fflff” and should end up at (2, 2)
// Wall - The robot is on a 50×50 grid at location (1, 1) and facing NORTH. The robot is given the commands “fflff” and should end up at (1, 0)
// Obstacle - The robot is on a 100×100 grid at location (50, 50) and facing NORTH. The robot is given the commands “fflffrbb” but there is an obstacle at (48, 50) and should end up at (48, 49)
class RobotTest extends \PHPUnit_Framework_TestCase
{
    public function testWalk()
    {
        $robot = new Robot(new Grid(100, 100), array(0, 0), Robot::SOUTH);
        $robot->executeCommands('fflff');
        $this->assertEquals(array(2, 2), $robot->getPosition());
        $this->assertFalse($robot->isBlocked());
    }

    public function testWall()
    {
        $robot = new Robot(new Grid(50, 50), array(1, 1));
        $robot->executeCommands('fflff');
        $this->assertEquals(array(1, 0), $robot->getPosition());
        $this->assertTrue($robot->isBlocked());
    }

    public function testWallWrap()
    {
        $robot = new Robot(new Grid(50, 50, true), array(1, 1));
        $robot->executeCommands('fflff');
        $this->assertEquals(array(49, 49), $robot->getPosition());
        $this->assertFalse($robot->isBlocked());
    }
    
    public function testObstacle()
    {
        $grid = new Grid(100, 100);
        $grid->addObstacle(array(48, 50));
        $robot = new Robot($grid, array(50, 50));
        $robot->executeCommands('fflffrbb');
        $this->assertEquals(array(48, 49), $robot->getPosition());
        $this->assertTrue($robot->isBlocked());
    }
}
